void main(List<String> args) {
  print(rangeWithStep(1, 10, 2));
  print(rangeWithStep(11, 23, 3));
  print(rangeWithStep(5, 2, 1));
}

rangeWithStep(starNum, finishNum, step) {
  var rangeArr = [];
  if (starNum > finishNum) {
    var currentNum = starNum;
    for (var i = 0; currentNum >= finishNum; i++) {
      rangeArr.add(currentNum);
      currentNum -= step;
    }
  } else if (starNum < finishNum) {
    var currentNum = starNum;
    for (var i = 0; currentNum <= finishNum; i++) {
      rangeArr.add(currentNum);
      currentNum += step;
    }
  } else {
    return 1;
  }
  return rangeArr;
}
